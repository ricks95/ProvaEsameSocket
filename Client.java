import java.net.*;
import java.io.*;

public class Client {
	public static void main(String[] args) {
		int score = 0;
		int game = 0;
		String result = "";
		String server = "localhost";
		int serverPort = 56789;
		int buffer_dim = 1;
		byte[] buffer = new byte[buffer_dim];
		String user_message = "";

		try {
			if (args.length == 2) {
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				DatagramSocket clientSocket = new DatagramSocket();
				server = args[0];
				serverPort = Integer.parseInt(args[1]);
				DatagramPacket dp;
				SocketAddress serverAddr = new InetSocketAddress(server, serverPort);

				System.out.println("Inserisci 'p' per iniziare una partita");

				while (!((user_message = br.readLine()).equalsIgnoreCase("p"))) {
					System.out.println("Inserisci 'p' per iniziare una partita");
				}

				dp = new DatagramPacket(user_message.getBytes(), 1, serverAddr);
				clientSocket.send(dp);
				clientSocket.receive(dp);

				if ((char)(dp.getData()[0]) == 'k') {
					System.out.println("Inserisci 'f', 's', 'c' oppure '.' per terminare.");

					while (!((user_message = br.readLine()).equalsIgnoreCase("."))) {
						dp = new DatagramPacket(user_message.getBytes(), 1, serverAddr);
						clientSocket.send(dp);
						clientSocket.receive(dp);

						char server_message = (char)(dp.getData()[0]);
						char switchUser = user_message.charAt(0);

						switch (switchUser) {
							case 'f':
							if (server_message == 's') {
								game = -1;
							}
							else {
								game = 1;
							}
							break;
							case 's':
							if (server_message == 'c') {
								game = -1;
							}
							else {
								game = 1;
							}
							break;
							case 'c':
							if (server_message == 'f') {
								game = -1;
							}
							else {
								game = 1;
							}
							break;
							default:
								System.out.println("Inserisci 'f', 's', 'c' oppure '.' per terminare.");
								continue;
						}
						result = (game == -1) ? "Vince Server!" : "Vince Client!";
						if (server_message == switchUser) {
							game = 0;
							result = "Pareggio";
						}
						System.out.println(result);
						score += game;
					}

					user_message = (score > 0) ? "i" : "y";

					dp = new DatagramPacket(user_message.getBytes(), 1, serverAddr);
					clientSocket.send(dp);
					clientSocket.receive(dp);
					if ((char)dp.getData()[0] == 'b') {
						System.out.println("Ciao server.");
					}
					else {
						System.out.println("Ricevuto messaggio sbagliato ma termino.");
					}
				}

				else {
					System.out.println("Ricevuto messaggio sbagliato.");
				}
			}
			else {
				System.out.println("Errore di utilizzo, esegui con $>java Client <hostname> <port_number>");
			}
		}
		catch (SocketException socketExc) {
			System.err.println("Errore nella apertura della socket.");
			System.exit(-1);
		}
		catch (IOException ioExc) {
			System.err.println("Errore IO generico.");
			System.exit(-1);
		}
		catch (NumberFormatException numberExc) {
			System.err.println("Impossibile processare il numero di porta, inserisci un intero.");
			System.exit(-1);
		}
		catch (IllegalArgumentException illegalArg) {
			System.err.println("Il numero di porta dev'essere 1 <= n <= 65536.");
			System.exit(-1);
		}
	}
}
