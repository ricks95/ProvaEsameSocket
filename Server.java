import java.net.*;
import java.io.*;
import java.util.*;

public class Server {
	public static void main(String[] args) {
		int cs = 0;
		int cw = 0;
		char message = Character.MIN_VALUE;
		Random prng = new Random();
		int buffer_dim = 1;
		byte[] buffer = new byte[buffer_dim];
		DatagramPacket dp = new DatagramPacket(buffer, buffer_dim);

		try {
			DatagramSocket serverSocket = new DatagramSocket();
			System.out.println("Server Port: " + serverSocket.getLocalPort());

			while (true) {
				serverSocket.receive(dp);
					char character = (char)dp.getData()[0];
					switch (character) {
						case 'p':
							cs++;
							System.out.println("New client: " + dp.getAddress());
							message = 'k';
							break;
						case 'f':
						case 's':
						case 'c':
							int rand = prng.nextInt(3);
							if (rand == 0) {
								message = 'f';
							}
							if (rand == 1) {
								message = 's';
							}
							if (rand == 2) {
								message = 'c';
							}
							System.out.println("Client: " + character + "; Server: " + message);
							break;
						case 'y':
							cw++;
						case 'i':
							message = 'b';
							System.out.println("Played: " + cs + "; Won: " + cw);
					}
					SocketAddress clientAddr = dp.getSocketAddress();
					buffer[0] = (byte)message;
					dp = new DatagramPacket(buffer, 1, clientAddr);
					serverSocket.send(dp);
			}
		}
		catch (SocketException socketExc) {
			System.err.println("Errore nella apertura della socket.");
			System.exit(-1);
		}
		catch (IOException ioExc) {
			System.err.println("Errore di comunicazione.");
			System.exit(-1);
		}
	}
}
